﻿using System;
using NAudio.Midi;
using WindowsInput;
using WindowsInput.Native;

namespace MIDIKeyboardTranslator
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int selectedDeviceIndex = 0;
            var midiIn = new MidiIn(selectedDeviceIndex);
            
            midiIn.MessageReceived += midiIn_MessageReceived!;
            midiIn.Start();

            Console.ReadLine();
            midiIn.Stop();
        }
        
        private static void midiIn_MessageReceived(object sender, MidiInMessageEventArgs e)
        {
            var me = e.MidiEvent;
            if (me.CommandCode == MidiCommandCode.NoteOn)
            {
                var note = (NoteEvent) me;
                VirtualKeyCode vkey = VirtualKeyCode.None;
                
                switch (note.NoteNumber)
                {
                    case 36: vkey = VirtualKeyCode.VK_C;
                        break;
                    case 38: vkey = VirtualKeyCode.VK_D;
                        break;
                    case 40: vkey = VirtualKeyCode.VK_E;
                        break;
                    case 41: vkey = VirtualKeyCode.VK_F;
                        break;
                    case 43: vkey = VirtualKeyCode.VK_G;
                        break;
                    case 45: vkey = VirtualKeyCode.VK_A;
                        break;
                    case 47: vkey = VirtualKeyCode.VK_B;
                        break;
                }

                new InputSimulator().Keyboard.KeyPress(vkey);
            }
        }
    }
}